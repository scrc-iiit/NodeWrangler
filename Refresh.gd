extends Button


@onready var NodeList = null
func _ready():
	NodeList = get_parent().get_node("mainWiSUN")
	connect("pressed", _button_pressed)

func _button_pressed():
	NodeList.recompute_links()
