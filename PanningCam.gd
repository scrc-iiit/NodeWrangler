extends Camera2D
class_name PanningCamera2D

const MIN_ZOOM: float = 0.2
const MAX_ZOOM: float = 1.5
const ZOOM_RATE: float = 8.0
const ZOOM_INCREMENT: float = 0.1
var _target_zoom: float = 1.0
var target_pos = Vector2.ZERO
var NodeList
var move_camera = false
var automated_camera = true
var keep_zooming_in = false
var keep_zooming_out = false

func _ready():
	NodeList = get_parent().get_node("mainWiSUN").NodeList
	
func _physics_process(delta: float) -> void:
	zoom = lerp(zoom, _target_zoom * Vector2.ONE, ZOOM_RATE * delta)
	set_physics_process(not is_equal_approx(zoom.x, _target_zoom))
	
	

func _process(delta):
	if(move_camera):
		offset -= (get_global_mouse_position() - target_pos)*delta*10
		$Sprite2D.position = offset
		$GpuParticles2d.position = offset
	if(keep_zooming_in):
		zoom_out()
	elif(keep_zooming_out):
		zoom_in()
	if(automated_camera):
		if(len(get_parent().get_node("mainWiSUN").NodeList) > 0):
			position = lerp(position,get_parent().get_node("mainWiSUN").NodeList[0].position,5*delta)
	
func _refresh_node_list():
	NodeList = get_parent().get_node("mainWiSUN").NodeList
	print(len(NodeList))
	

func _input(event: InputEvent) -> void:
	if(event.is_action_pressed("ZOOMIN")):
		keep_zooming_in = true
	elif(event.is_action_released("ZOOMIN")):
		keep_zooming_in = false
	if(event.is_action_pressed("ZOOMOUT")):
		keep_zooming_out = true
	elif(event.is_action_released("ZOOMOUT")):
		keep_zooming_out = false
		
	if event is InputEventMouseButton:
		if event.is_pressed():
			if event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
				zoom_in()
			if event.button_index == MOUSE_BUTTON_WHEEL_UP:
				zoom_out()
		if(event.button_index == MOUSE_BUTTON_LEFT):
			#automated_camera = false
			if(event.is_action_pressed("LMB")):
				move_camera = true
				target_pos = get_global_mouse_position()
			if(event.is_action_released("LMB")):
				move_camera = false



func zoom_in() -> void:
	_target_zoom = max(_target_zoom - ZOOM_INCREMENT, MIN_ZOOM)
	set_physics_process(true)


func zoom_out() -> void:
	_target_zoom = min(_target_zoom + ZOOM_INCREMENT, MAX_ZOOM)
	set_physics_process(true)

func focus_position(target_position: Vector2) -> void:
	var tween = create_tween()
	tween.tween_property(self, "position",target_position,0.2).set_ease(Tween.EASE_OUT)
