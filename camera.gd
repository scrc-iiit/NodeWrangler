extends Camera2D

@onready var NodeList = null

func process_camera():
	var max_pos_offset = 0
	var centroid = Vector2.ZERO
	for node in NodeList:
		if(node.conn_list < 1):
			continue
		var dist = node.global_position.distance_to(self.offset - get_viewport().get_visible_rect().size/2)
		centroid += node.global_position
		if(max_pos_offset < dist):
			max_pos_offset = dist
	centroid = centroid/NodeList.size()
	self.zoom = remap(max_pos_offset,1000,3509,1,0.2) * Vector2.ONE
	self.position = centroid #- get_viewport().get_visible_rect().size/2

func process_movement():
	pass

func _ready():
	NodeList = get_parent().get_node("mainWiSUN").NodeList
# Called every frame. 'delta' is the elapsed time since the previous frame.

