extends RigidBody2D

@export var node_name = "N"
@export var sig_power = 0;
@export var conn_list = 0;

func _ready():
	$graphic.texture = load("res://icons/wifi_green.png")
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$label.text = node_name
	if(conn_list < 1):
		$graphic.texture = load("res://icons/wifi_dead.png")
	elif(node_name == "Border Router"):
		$graphic.texture = load("res://icons/border.png")
	else:
		$graphic.texture = load("res://icons/wifi_green.png")
	
