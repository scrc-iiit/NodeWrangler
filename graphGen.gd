extends Node

var nodePrefab = preload("res://lenode.tscn")
var linkPrefab = preload("res://link.tscn")

@export var RADIUS = 10;
@export var rot_offset = 0;
@export var wisun_adj_link = "https://pastebin.com/raw/QFAwsH7h"
var just_init = true
var _timer
@onready var adj_matrix = [
	[0,	1,	0,	1,	0,	0,	0,	0,	1,	1],
	[1,	0,	0,	0,	0,	0,	0,	0,	0,	0],
	[0,	0,	0,	0, 	1,	0,	0,	0,	0,	0],
	[1,	0,	0,	0, 	1,	0,	0,	1,	0,	0],
	[0,	0,	1,	1, 	0,	1,	1,	0,	0,	0],
	[0,	0,	0,	0, 	1,	0,	0,	0,	0,	0],
	[0,	0,	0,	0, 	1,	0,	0,	0,	0,	0],
	[0,	0,	0,	1, 	0,	0,	0,	0,	0,	0],
	[1,	0,	0,	0, 	0,	0,	0,	0,	0,	0],
	[1,	0,	0,	0, 	0,	0,	0,	0,	0,	0]
]
@onready var NodeList = []
@onready var LinkList = []
@onready var LinesList = []
@onready var NodeNameList = []

func process_csv_adj():
	pass


func print_adj_matrix(in_adj,stdout=true):
	var returnable = ""
	var tot_len = in_adj.size()
	for i in range(tot_len):
		if(in_adj.size() == in_adj[i].size()):
			continue
		else:
			print("Malformed Adj matrix %dx%d"%[in_adj.size(),in_adj[i].size()])
			return -1
	var to_print_line = "\t"
	
	
	for ndx in range(tot_len):
		to_print_line += "\tN%s" % ndx
	if(stdout):
		print(to_print_line + "\n")
		
	returnable += to_print_line + "\n"
	
	for i in range(tot_len):
		to_print_line = "N%s\t|\t" % i
		for j in range(tot_len):
			to_print_line += str(in_adj[i][j]) + "\t"
		returnable += to_print_line + "|\n"
		if(stdout):
			print(to_print_line + "|")
	return returnable

func create_nodes(in_adj):
	if(len(in_adj) < 1):
		$"../ParallaxBackground/debug".text = "Unable to process matrix"
		return 0
	var screen_size = get_viewport().get_visible_rect().size
	var tot_len = in_adj.size()
	if(typeof(print_adj_matrix(in_adj)) == TYPE_INT):
		print("Cannot create NodeList")
		return -1
	for i in range(tot_len):
		var cur_node = nodePrefab.instantiate()
		cur_node.set_name("N%s"%i)
		get_node("Nodes").add_child(cur_node)
		cur_node.position = screen_size/2 + RADIUS * Vector2(cos(2*PI*i/tot_len),sin(2*PI*i/tot_len))
		cur_node.get_node("graphic").z_index = 10
#		cur_node.node_name = "N%s"%i
		cur_node.node_name = NodeNameList[i]
		for neigh in in_adj[i]:
			cur_node.conn_list += neigh
		if(i==0):
			cur_node.node_name = "Border Router"
		NodeList.append(cur_node)
	return 0
	
	
func create_links(in_adj):
	if(len(in_adj) < 1):
		$"../ParallaxBackground/copyright/Status".texture = load("res://icons/offline.png")
		$"../ParallaxBackground/debug".text = "Error forming matrix"
		return 0
	var screen_size = get_viewport().get_visible_rect().size
	var tot_len = in_adj.size()		
	for i in range(tot_len):
		for j in range(i,tot_len):
			if(i==j):
				continue
			if(in_adj[i][j] == 1):
				var cur_link = DampedSpringJoint2D.new()
				cur_link.set_name("N%sN%s"%[i,j])
				get_node("Links").add_child(cur_link)
				# set properties
				
				cur_link.node_a = NodeList[i].get_path()
				cur_link.node_b = NodeList[j].get_path()
				cur_link.position = NodeList[i].global_position 
				var diff_pos = NodeList[j].global_position - NodeList[i].global_position
				cur_link.rotation = diff_pos.angle() - PI/2
				
				cur_link.length = NodeList[i].position.distance_to(NodeList[j].position)
				
				cur_link.rest_length = NodeList[i].get_node("colshape").shape.radius*2
				cur_link.stiffness = 64
				cur_link.damping = 1.5
				cur_link.disable_collision = false
				
				#line renderer between
				var lr = Line2D.new()
				lr.set_name("lr")
				lr.width = 4
				lr.add_point(NodeList[i].position)
				lr.add_point(NodeList[j].position)
				
				lr.joint_mode = Line2D.LINE_JOINT_ROUND
				lr.begin_cap_mode = Line2D.LINE_CAP_ROUND
				lr.end_cap_mode = Line2D.LINE_CAP_ROUND
				lr.default_color = Color.DARK_GRAY
				lr.z_index = -1
				get_node("Links").add_child(lr)
				LinkList.append(cur_link)
				LinesList.append(lr)


func delete_nodes():
	for node in get_node("Nodes").get_children():
		get_node("Nodes").remove_child(node)

func delete_links():
	for node in get_node("Links").get_children():
		get_node("Links").remove_child(node)

func recompute_links():
	LinesList = []
	LinkList = []
	delete_links()
	create_links(adj_matrix)
	

func randomize_links():
	var new_adj = []
	var random_total = randi_range(3,15)
	for i in range(random_total):
		var inner_array = []
		for inner in range(random_total):
			inner_array.append(0)
		new_adj.append(inner_array)
		$"../ParallaxBackground/debug".text = str(new_adj)
	
	for i in range(random_total):
		for j in range(i,random_total):
			if(i==j):
				continue
			else:
				new_adj[i][j] = 1 if randi_range(0,10) > 8 else 0
				new_adj[j][i] = new_adj[i][j]
		$"../ParallaxBackground/debug".text = "Symmetric Matrix\n\n" + str(new_adj)
				
	update_nodes(new_adj)
	
	if(create_nodes(adj_matrix) == 0):
		create_links(adj_matrix)

func update_nodes(adj_matrix):
	LinesList = []
	NodeList = []
	LinkList = []
	delete_nodes()
	delete_links()
	$"../ParallaxBackground/debug".text = "Creating Nodes ..."
	if(create_nodes(adj_matrix) == 0):
		create_links(adj_matrix)	



func _ready():
	var res = $HTTP.request(wisun_adj_link)
	_timer = Timer.new()
	add_child(_timer)
	$"../ParallaxBackground/copyright/Status".texture = load("res://icons/offline.png")
	_timer.connect("timeout",_on_Timer_timeout)
	_timer.set_wait_time(5.0)
	_timer.set_one_shot(false) # Make sure it loops
	_timer.start()

	if(res != OK):
		$"../ParallaxBackground/debug".text = "Unable to connect to API server\n" + wisun_adj_link
		
func _on_Timer_timeout():
	var res = $HTTP.request(wisun_adj_link)
	if(res != OK):
		$"../ParallaxBackground/debug".text = "Unable to connect to API server\n" + wisun_adj_link

func handle_turbulence():
	var TIME =  Time.get_ticks_msec()
	for node in NodeList:
		node.position += 0.1*Vector2(sin_noise(TIME/10000.0),sin_noise(TIME/10000.0+PI/2))

func sin_noise(k):
	var noise = 0.0
	for i in range(0,5):
		noise += (4*i + 2*i*i) * sin(i*k)
	return noise/100.0

func process_lines():
	for i in range(LinesList.size()):
		var linknode = LinkList[i]
		var linenode = LinesList[i]
		linenode.set_point_position(0,get_node(linknode.node_a).position)
		linenode.set_point_position(1,get_node(linknode.node_b).position)


	
func _process(delta):
	process_lines()
	handle_turbulence()

func _on_randomize_pressed():
	$"../ParallaxBackground/debug".text = "Generating Adjoint Matrix"
	randomize_links()
	$"../ParallaxBackground/debug".text = "Debug Drawing\n" + print_adj_matrix(adj_matrix)




func _on_check_button_toggled(button_pressed):
	$"../ParallaxBackground/debug".visible = button_pressed;
	$"../ParallaxBackground/Randomize".visible = button_pressed;
	$"../ParallaxBackground/Refresh3".visible = button_pressed;


func _on_requests_request_completed(result, response_code, headers, body):
	if(response_code > 200):
		$"../ParallaxBackground/copyright/Status".texture = load("res://icons/offline.png")
		$"../ParallaxBackground/debug".text = "Error processing server data"
		return 0
	var res = body.get_string_from_utf8().replace("\r","");
	var lines = res.split("\n")
	$"../ParallaxBackground/debug".text = "Processing Data ..."
	var groupname=""
	var ADJ_MATRIX_FILE = []
	for index in range(len(lines)):
		if(len(lines[index]) < 1):
			continue
		var inner = []
		var line = lines[index]
		var splits = line.split(",")
		for i in splits:
			if(index < 1):
				NodeNameList.append(i)
			else:
				inner.append(i.to_int())
		if(index >= 1):
			ADJ_MATRIX_FILE.append(inner)
			
	adj_matrix = ADJ_MATRIX_FILE.slice(0,ADJ_MATRIX_FILE.size())
	if(just_init):
		update_nodes(adj_matrix)
		just_init=false
	else:
		recompute_links()
	$"../ParallaxBackground/debug".text = "Success\n" + res
	$"../ParallaxBackground/copyright".text = wisun_adj_link
	$"../ParallaxBackground/copyright/Status".texture = load("res://icons/online.png")


func _on_refresh_2_pressed():
	var res = $HTTP.request(wisun_adj_link)
	$"../ParallaxBackground/debug".text = "Connecting to server..."


func _regen_links_on_pressed():
	if(len(adj_matrix) > 1):
		update_nodes(adj_matrix)
	else:
		$"../ParallaxBackground/copyright/Status".texture = load("res://icons/offline.png")
